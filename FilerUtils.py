
import os, re

DEBUG = False

def capitalize(phrase):
    """Set first character of every word in phrase to uppercase"""
    new_words = [word.capitalize() for word in phrase.split()]
    return " ".join(new_words)

def debug_print(*msg):
    """Debug print"""
    if DEBUG:
        print('[DEBUG]', *msg)

def get_first_num(phrase):
    """Get first number from string that contains a number"""
    num = ''
    started = False
    for c in phrase:
        is_num = c.isnumeric()
        if not started:
            if is_num:
                started = True
        if started:
            if not is_num:
                break
            num += c
    return num

def remove_empty_dirs(directories):
    for d in directories:
        try:
            os.rmdir(d)
            debug_print("Removing '{}'".format(d))
        except Exception as e:
            pass

def get_filelist_p(extensions, path):
    """Return list of files of that match given extensions"""
    files = [ os.path.join(path, f) for f in os.listdir(path)]
    return get_filelist_f(extensions, files)

def get_filelist_f(extensions, files):
    """Underlying logic to get_filelist() function, files 
    list must be populated with absolute file paths"""
    matching_files = []
    ext_list = extensions.replace(' ', '').lower().split(',')
    dirs = []

    for f in files:
        ext = os.path.splitext(f)[1].lower()
        abs_f = os.path.abspath(f)
        if ext in ext_list and ext != '': 
            matching_files.append(abs_f)
        elif os.path.isdir(abs_f):
            dirs.append(abs_f)

    #debug_print('\t\tpath =>', path)
    #debug_print('\t\textensions =>', extensions)
    #debug_print('\t\text_list =>', ext_list)
    #debug_print("\t\tfiles => [\n\t\t\t'" + 
            #"'\n\t\t\t'".join(files) + "'\n\t\t\t]")
    #debug_print("\t\tdirectories => [\n\t\t\t'" +
            #"'\n\t\t\t'".join(directories) + "'\n\t\t\t]")
    return matching_files, dirs

def set_debug(debug):
    global DEBUG
    DEBUG = debug 

def get_debug():
    return DEBUG

def get_delimiter(name):
    """Get delimiter used in filename, usually '.' or ' '"""
    dot_count = name.count('.')
    space_count = name.count(' ')
    if dot_count > space_count:
        return '.'
    elif space_count > dot_count:
        return ' '
    else:
        return None

def get_show_meta(name, delim):
    """Get show information from filename"""
    season = ''
    episode = ''
    other = ''
    show = ''
    show_done = False
    low_name = name.lower()
    split_name = name.split(delim)
    # get info from format "<Name> Season<Season Number> Episode <Episode Number>.<ext>"
    if 'episode' in low_name:
        debug_print('\t', split_name)
        for n in split_name:
            if not show_done:
                if n.lower() == 'season':
                    indx = split_name.index(n)
                    season = split_name[indx + 1]
                    episode = split_name[indx + 3]
                    show_done = True
                    continue
                if n.lower() == 'episode':
                    indx = split_name.index(n)
                    season = '01' #assume it's season 1 if no season specified
                    episode = split_name[indx + 1]
                    show_done = True
                    continue
                show += n + ' '
    else:
        # get info from format "<Name>.S<Season Number>E<Episode Number>.<ext>"
        for n in split_name:
            s = re.match(r'(?i)s\d{1,2}', n)
            e = re.search(r'(?i)e\d{1,3}', n)
            if s and e:
                show_done = True
                season = get_first_num(s.group())
                episode = get_first_num(e.group())
                continue
            if not show_done:
                show += n + ' '
            else:
                other += n + ' '
    s_len = len(season)
    e_len = len(episode)
    if s_len < 2 and s_len > 0:
        season = '0' + season
    if e_len < 2 and e_len > 0:
        episode = '0' + episode 
    meta = {}
    meta['season'] = season
    meta['episode'] = episode 
    meta['show'] = capitalize(show.strip())
    meta['other'] = other.strip()
    return meta

def get_movie_meta(name, delim):
    """Get movie information from filename"""
    year = ''
    title = ''
    quality= ''
    other = ''
    title_done = False
    name = name.replace('(', ''
            ).replace(')', ''
            ).replace('[', ''
            ).replace('[', '')
    for n in name.split(delim):
        yObj = re.match(r'^\d{4}$', n)
        if yObj:
            year = n
            title_done = True
            continue
        qObj = re.match(r'^\d{3,4}(?i)p$', n)
        if qObj:
            quality = n
            title_done = True
            continue
        if not title_done:
            title += n + ' '
        else:
            other += n + ' '
    meta = {}
    meta['title'] = capitalize(title.strip())
    meta['other'] = other.strip()
    meta['year'] = year
    meta['quality'] = quality
    return meta

