#!/bin/python3

import os, argparse

import FilerUtils
from Filer import AutoFiler
from AudioFiler import MP3Filer
from VideoFiler import ShowFiler, MovieFiler

def run_AudioFiler(src_path, dst_path, rename, verbose):
    print("[*] Organising audio files...")
    filer = MP3Filer(dst_path, rename, verbose)
    filer.organise_files(src_path)
    print("[*] Done")

def run_VideoFiler(src_path, dst_path, rename, verbose):
    print("[*] Organising video files...")
    af = AutoFiler(dst_path, rename, verbose)
    af.add_filer(ShowFiler(dst_path, rename, verbose))
    af.add_filer(MovieFiler(dst_path, rename, verbose))
    af.organise_files(src_path)
    print("[*] Done")

def run_allFiler(src_path, dst_path, rename, verbose):
    af = AutoFiler(dst_path, rename, verbose)
    af.add_filer(ShowFiler(dst_path, rename, verbose))
    af.add_filer(MovieFiler(dst_path, rename, verbose))
    af.add_filer(MP3Filer(dst_path, rename, verbose))
    af.organise_files(src_path)

def get_cli_args():
    parser = argparse.ArgumentParser(
            description='Neatly organises files into directories based'
                    + ' on their metadata and naming conventions')
    parser.add_argument('-s', '--src_path', type=str, metavar='SOURCE',
            help='Source path of directory to organise', default='.')
    parser.add_argument('-d', '--dst_path', type=str, metavar='DESTINATION',
            help='Destination path where organised files will be stored', default='.')
    parser.add_argument('-f', '--final_dir', type=str, metavar='DIRECTORY', 
            help='Name of new directory to store organised', default='')
    parser.add_argument('-r', '--rename_off', action='store_true', 
            help='Do not rename any files')
    parser.add_argument('-v', '--verbose', action='store_true',
            help='Display information during organising process.')
    parser.add_argument('-D', '--debug', action='store_true',
            help='Display debug information during organising process.')
    parser.add_argument('-V', '--video', action='store_true',
            help='Organise video files.')
    parser.add_argument('-A', '--audio', action='store_true',
            help='Organise audio files.')
    parser.add_argument('-E', '--everything', action='store_true', 
            help='Organise everything.')
    args = vars(parser.parse_args())
    return args

def create_destination(final_dst):
    print("Debug Mode: {}\n".format(FilerUtils.get_debug()))
    if not os.path.exists(final_dst):
        print("Creating {}".format(final_dst))
        os.mkdir(final_dst)
    assert os.path.exists(final_dst),\
            "'{}' does not exist.".format(final_dst)

def main():
    args = get_cli_args()
    out_dir = args['final_dir']
    src = args['src_path']
    dst = args['dst_path']
    rename = not args['rename_off']
    verbose = args['verbose']
    debug = args['debug']
    video = args['video']
    audio = args['audio']
    org_all = args['everything']
    src_path = os.path.abspath(src)
    dst_path = os.path.abspath(dst)

    assert os.path.exists(src_path),\
            "Given SOURCE '{}' does not exist.".format(src_path)
    assert os.path.isdir(src_path),\
            "Given SOURCE '{}' is not a directory.".format(src_path)
    assert os.path.exists(dst_path),\
            "Given DESTINATION '{}' does not exist.".format(dst_path)
    assert os.path.isdir(dst_path),\
            "Given DESTINATION '{}' is not a directory.".format(dst_path)

    final_dst = os.path.join(dst_path, out_dir)

    FilerUtils.set_debug(debug)
    FilerUtils.debug_print("final_dst =>", final_dst)
    FilerUtils.debug_print("src_path =>", src_path)

    if org_all:
        create_destination(final_dst)
        run_allFiler(src_path, final_dst, rename, verbose)
    else:
        if video:
            create_destination(final_dst)
            run_VideoFiler(src_path, final_dst, rename, verbose)
        elif audio:
            create_destination(final_dst)
            run_AudioFiler(src_path, final_dst, rename, verbose)
    

if __name__ == '__main__':
    main()
