
import os, shutil
import re

from Filer import *
from FilerUtils import *

class VideoFiler(Filer):
    def __init__(self, dst_path, fdir='', rename=False, verbose=True):
        super(VideoFiler, self).__init__(os.path.join(dst_path, fdir), 
            '.mkv,.mp4,.avi', rename, verbose)

    def __file_subs__(self, path, file, file_dict):
        pass

class ShowFiler(VideoFiler):

    def __init__(self, dst_path, rename=False, verbose=True, fdir='Shows'):
        super(ShowFiler, self).__init__(dst_path, fdir, rename, verbose)

    def __retrieve_meta__(self, files):
        super(ShowFiler, self).__retrieve_meta__()
        meta_dict = {}
        for f in files:
            name = os.path.splitext(os.path.basename(f))[0]
            delim = get_delimiter(name)
            if delim is None:
                continue
            meta = get_show_meta(name, delim)

            # can't classify as show without season or episode
            if len(meta['season']) < 1 or len(meta['episode']) < 1:
                continue

            debug_print('\t\tshow =>', meta['show'])
            debug_print('\t\tseason =>', meta['season'])
            debug_print('\t\tepisode =>', meta['episode'])
            debug_print()
            meta_dict[f] = meta
           
        return meta_dict
            
    def __create_dirs__(self, files, file_dict, meta_dict):
        super(ShowFiler, self).__create_dirs__()
        dst_path = self.dst_path
        rename = self.rename
        for f in meta_dict:
            meta = meta_dict[f] 
            show = meta_dict[f].get('show')
            season = meta_dict[f].get('season')
            episode = meta_dict[f].get('episode')
            other = meta_dict[f].get('other')
           
            show_dir = os.path.join(dst_path, show)
            season_dir = os.path.join(show_dir, "S" + season)
            if not os.path.exists(show_dir):
                os.mkdir(show_dir)
            if not os.path.exists(season_dir):
                os.mkdir(season_dir)

            fname = os.path.basename(f)
            if rename:
                ext = os.path.splitext(fname)[1]
                fname = '{}.S{}E{}'.format(
                        show.replace(' ','.'), season, episode)
                if other != '':
                    fname += '.' + other.replace(' ', '.') +  ext
                else:
                    fname += ext
            file_dict[f] = []
            file_dict[f].append(season_dir)
            file_dict[f].append(fname)


class MovieFiler(VideoFiler):

    def __init__(self, dst_path, rename=False, verbose=True, fdir='Movies'):
        super(MovieFiler, self).__init__(dst_path, fdir, rename, verbose)

    def __retrieve_meta__(self, files):
        super(MovieFiler, self).__retrieve_meta__()
        meta_dict = {}
        for f in files:
            name = os.path.splitext(os.path.basename(f))[0]
            delim = get_delimiter(name)
            if delim is None:
                continue
            meta = get_movie_meta(name, delim)

            # can't classify as movie without year
            if meta['year'] == '':
                continue
            debug_print('\t\ttitle=>', meta['title'])
            debug_print('\t\tyear=>', meta['year'])
            debug_print('\t\tquality=>', meta['quality'])
            debug_print('\t\tother=>', meta['other'])
            debug_print()
            meta_dict[f] = meta
           
        return meta_dict
             
    def __create_dirs__(self, files, file_dict, meta_dict):
        super(MovieFiler, self).__create_dirs__()
        dst_path = self.dst_path
        rename = self.rename
        for f in meta_dict:
            meta = meta_dict[f] 
            title = meta_dict[f].get('title')
            quality = meta_dict[f].get('quality')
            year = meta_dict[f].get('year')
            other = meta_dict[f].get('other')
           
            year_dir = os.path.join(dst_path, year)
            if not os.path.exists(year_dir):
                os.mkdir(year_dir)
            movie_dir = os.path.join(year_dir, title)
            if not os.path.exists(movie_dir):
                os.mkdir(movie_dir)
            fname = os.path.basename(f)
            if rename:
                ext = os.path.splitext(fname)[1]
                fname = '{}.{}.{}'.format(title, year, quality)
                if other != '':
                    fname += '.' + other.replace(' ', '.') +  ext
                else:
                    fname += ext
            file_dict[f] = []
            file_dict[f].append(movie_dir)
            file_dict[f].append(fname)
