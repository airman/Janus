
import os, shutil
from FilerUtils import get_filelist_f, get_filelist_p, debug_print, remove_empty_dirs

class Filer(object):
    """
        Organises files in given path into directories based on their
        metadata. 
    """

    def __init__(self, dst_path, ext, rename, verbose):
        self.extensions = ext
        self.rename = rename
        self.verbose = verbose
        self.dst_path = dst_path
        self.file_count = 0
        self.directories = []
        if not os.path.exists(dst_path):
            os.mkdir(dst_path)

    def __print__(self, *msg):
        if self.verbose:
            print(*msg)

    def __retrieve_meta__(self, *args):
        self.__print__('\t[*] Retrieving metadata...')

    def __create_dirs__(self, *args):
        self.__print__('\t[*] Creating directories...')

    def __move_files__(self, file_dict, meta_dict):
        self.__print__('\t[*] Moving files...')
        for f in meta_dict:
            file_dir = file_dict[f][0]
            fname = file_dict[f][1]
            debug_print("\t\told fname =>", os.path.basename(f))
            fname = self.__enumerate_filename__(fname, file_dir)
            debug_print("\t\tnew fname =>", fname)
            dst_file = os.path.join(file_dir, fname)

            debug_print("\t\tmoving => [")
            debug_print("\t\t\t'%s'" % f)
            debug_print("\t\t\t'%s'" % dst_file)
            debug_print("\t\t\t]")
            shutil.move(f, dst_file)

    def __enumerate_filename__(self, fname, path):
        count = 1
        temp_name = fname
        fname, ext = os.path.splitext(fname)
        while(True):
            abs_path = os.path.join(path, temp_name)
            if not os.path.exists(abs_path):
                print("path: '%s'" % abs_path)
                return temp_name
            temp_name = '%s_copy(%i)%s' % (fname, count, ext)
            count += 1
        return temp_name 


    def organise_files(self, src_path):
        self.__organise_files__(src_path, 0)

    def __non_recursive_organise__(self, files, src_path):
        file_count = len(files)
        if file_count > 0:
            self.__print__("\t[+] %i unorganised [%s] files found in \n\t\t'%s'. " 
                    % (len(files), self.extensions, src_path))
            file_dict = {}
            meta_dict = self.__retrieve_meta__(files)
            self.file_count += len(meta_dict)
            debug_print("\t\tmeta_dict keys=>", meta_dict.keys())
            self.__create_dirs__(files, file_dict, meta_dict)
            self.__move_files__(file_dict, meta_dict)
        else:
            self.__print__("\t[-] Skipping '%s'" % src_path) 
        self.__print__()

    def __organise_files__(self, src_path, count=0):
        """Organise files from given src_path into self.dst_path"""
        files, directories = get_filelist_p(self.extensions, src_path)
        self.__non_recursive_organise__(files, src_path)
        self.directories.extend(directories)
        dst_path = self.dst_path
        for d in directories:
            if d != dst_path:
                self.__organise_files__(d, count+1)

        if count == 0:
            remove_empty_dirs(directories)
            self.__print__("\t[+] %i [%s] files organised. "
                    % (self.file_count, self.extensions))

class AutoFiler(Filer):

    """"""
    def __init__(self, dst_path, rename, verbose):
        super(AutoFiler, self).__init__(dst_path, '', rename, verbose)
        self.filers = []
        self.directories = []

    def add_filer(self, filer):
        self.extensions += filer.extensions + ','
        self.filers.append(filer)
    
    def __non_recursive_organise__(self, files, src_path):
        pass

    def __organise_files__(self, src_path, count=0):
        """Organise files from given src_path into self.dst_path"""
        files, directories = get_filelist_p(self.extensions, src_path)
        filer_files = {}
        self.directories.extend(directories)
        for filer in self.filers:
            files_ = get_filelist_f(filer.extensions, files)[0]
            filer.__non_recursive_organise__(files_, src_path)
        dst_path = self.dst_path
        for d in directories:
            if d != dst_path:
                self.__organise_files__(d, count+1)

        if count == 0:
            for filer in self.filers:
                self.file_count += filer.file_count
            remove_empty_dirs(self.directories)
            self.__print__("\t[+] %i %s files organised. "
                    % (self.file_count, list(set(tuple(self.extensions.split(','))))))

