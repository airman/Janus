import os, shutil

import mutagen
import mutagen.mp3

from Filer import *
from FilerUtils import *

class MP3Filer(Filer):
    """
        Organises mp3 files into directories based on metadata.
    """

    def __init__(self, dst_path, rename=True, verbose=True, fdir='Music'):
        super(MP3Filer, self).__init__(os.path.join(dst_path, fdir), '.mp3', rename, verbose) 

    def __retrieve_meta__(self, files):
        super(MP3Filer, self).__retrieve_meta__()
        meta = {}
        for f in files:
            try:
                meta[f] = mutagen.mp3.Open(f)
            except Exception as e:
                meta[f] = {}
        return meta
                    
    def __new_filename__(self, fname, meta):
        default_track_number = ''
        default_track_name = 'Unnamed Track'
        track_name = capitalize(str(meta.get('TIT2', default_track_name)))
        track_number = str(meta.get('TRCK', default_track_number))
        if track_name != default_track_name:
            fname = ''
            if track_number != default_track_number:
                track_number = int(get_first_num(track_number))
                fname += str(track_number) + '. '
            fname += track_name + '.mp3'

        # remove troublesome characters
        fname = fname.replace('\\', ' ')
        fname = fname.replace('\'', '')
        fname = fname.replace('\"', '')
        for c in ('/', '!', '*', '$', '`'):
            fname = fname.replace(c, '')
        return fname

    def __create_dirs__(self, files, file_dict, meta_dict):
        super(MP3Filer, self).__create_dirs__(files, file_dict)
        dst_path = self.dst_path
        default_artist = 'Unknown Artist'
        default_album = 'Unknown Album'
        default_year = ''
        default_genre = 'Unknown Genre'
        rename = self.rename

        for f in files:
            meta =  meta_dict[f]
            artist = capitalize(str(meta.get('TPE1', default_artist))).replace('/', ' & ')\
                    .replace('*', '')
            album = capitalize(str(meta.get('TALB', default_album))).replace('/', ' & ')\
                    .replace('*', '')

            year = str(meta.get('TDRC', default_year))
            genre = str(meta.get('TCON', default_genre)).replace('/', ' & ')\
                    .replace('*', '')


            debug_print("\t\tartist =>", artist)
            debug_print("\t\talbum =>", album)
            debug_print("\t\tyear =>", year)
            debug_print("\t\tgenre =>", genre)
            debug_print()

            artist_dir = os.path.join(dst_path, artist)
            album_dir = os.path.join(artist_dir, album)

            if not os.path.exists(artist_dir):
                os.mkdir(artist_dir)
            if not os.path.exists(album_dir):
                os.mkdir(album_dir)
            
            fname = os.path.basename(f)
            if rename:
                fname = self.__new_filename__(fname, meta)

            file_dict[f] = []
            file_dict[f].append(album_dir)
            file_dict[f].append(fname)

